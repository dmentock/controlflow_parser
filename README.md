# Controlflow Parser

Example call:

`python3 debug.py --args ~/env/DAMASK/bin/DAMASK_grid -w ~test_/test_grid_parse_loadcase_loadc0/ -l load.yaml -g simple.vti -m material.yaml`

Internally calls:

`gdb -ex "source gdb_extension.py" --args ~/env/DAMASK/bin/DAMASK_grid -w ~/test_/test_grid_parse_loadcase_loadc0/ -l load.yaml -g simple.vti -m material.yaml | tee gdb_output.log`

Control flow output can later be parsed to visualize it and make the traced state searchable and navigatable