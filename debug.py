import sys
import subprocess
from pathlib import Path
import os

sourcefile_dir = f'{os.getcwd()}/src/grid'
cache_funcs = False

def parse_args():
    global cache_funcs
    gdbargs = []
    program_args = []
    args = sys.argv[1:]

    while args:
        if args[0] == '--gdbargs':
            args.pop(0)
            while args and not args[0].startswith('--'):
                gdbargs.append(args.pop(0).strip())
        elif args[0] == '--args':
            args.pop(0)
            while args and not args[0].startswith('--'):
                program_args.append(args.pop(0).strip())
        else:
            raise ValueError(f"Unsupported flag {args[0]}")
    return gdbargs, program_args

def main():
    gdbargs, program_args = parse_args()
    gdb_command = [
        "gdb",
        "-ex", "source gdb_extension.py",
        "-ex", "run",
        "-ex", "q"
    ] + gdbargs + \
    ["--args"] + program_args

    with open(Path(Path(__file__).parents[0], "gdb_output.log"), "w") as log_file:
        process = subprocess.Popen(gdb_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding = 'utf-8')
        stdout,stderr = process.communicate(timeout=1000)
        log_file.write(stdout)
        if process.returncode != 0:
            print(stdout)
            print(stderr)

if __name__ == "__main__":
    main()
