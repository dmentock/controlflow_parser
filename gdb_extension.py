import gdb
from pathlib import Path

log_file_path = Path(Path(__file__).parents[0], "func_call_history.txt")
    
class LogBP(gdb.Breakpoint):
    def __init__(self, spec, bp_type):
        self.spec = spec
        super().__init__(spec, gdb.BP_BREAKPOINT, internal=False)
        self.silent = True
        self.bp_type = bp_type
        self.called_once = False
  
    def stop(self):
        if not self.called_once and self.bp_type == 'enter':
            self.called_once = True
            perform_return_actions(self.spec)
        frame = gdb.selected_frame()
        if frame:
            sal = frame.find_sal()
            if sal and sal.symtab and Path(sal.symtab.filename).name:
                with open(log_file_path, 'a') as log_file:
                    log_file.write(f"{self.bp_type}: {frame.name()} in file {sal.symtab.filename}\n")
        return False

def perform_return_actions(func_name):
    disassembly = gdb.execute(f"disassemble {func_name}", to_string=True)
    for line in disassembly.splitlines():
        if ':	ret' in line:
            print("aye")
            address = line.split()[0]
            if address:
                LogBP(f'*{address}', 'exit')

with open(log_file_path, 'w') as f:
    f.write('')

with open('funcs_to_trace.txt', 'r') as f:
    funcs_to_trace = f.read()

for func_name in funcs_to_trace.split('\n'):
    LogBP(func_name, 'enter')